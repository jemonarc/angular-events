import { DebugElement, NO_ERRORS_SCHEMA } from "@angular/core"
import { ComponentFixture, TestBed } from "@angular/core/testing"
import { By } from "@angular/platform-browser"
import { CollapsibleWellComponent } from "src/app/common"
import { AuthService } from "src/app/user/auth.service"
import { UpvoteComponent, VoterService } from "."
import { DurationPipe, ISession } from ".."
import { SessionListComponent } from "./session-list.component"


describe('SessionListComponentIs', () => {
    let component: SessionListComponent
    let mockAuthService, mockVoterService

    beforeEach(() => {
        component = new SessionListComponent(mockAuthService, mockVoterService)
    })

    describe('ngOnChanges', () => {
        it('should filter the sessions correctly', () => {
            component.sessions = <ISession[]>[
                {'name': 'session 1', level: 'intermediate' },
                {'name': 'session 2', level: 'intermediate' },
                {'name': 'session 3', level: 'beginner' }
            ]

            component.filterBy = 'intermediate'
            component.sortBy = 'name'
            component.ngOnChanges()

            expect(component.visibleSessions.length).toBe(2)
        })

        it('should sort the sessions correctly', () => {
            component.sessions = <ISession[]>[
                {'name': 'session 1', level: 'intermediate' },
                {'name': 'session 3', level: 'intermediate' },
                {'name': 'session 2', level: 'beginner' }
            ]

            component.filterBy = 'all'
            component.sortBy = 'name'
            component.eventId = 3
            component.ngOnChanges()

            expect(component.visibleSessions[2].name).toBe('session 3')
        })
    })
})

// integrated test
describe('SessionListComponent', () => {
    let mockAuthService,
        mockVoterService,
        fixture: ComponentFixture<SessionListComponent>,
        component: SessionListComponent,
        element: HTMLElement,
        debugEl: DebugElement
    
        beforeEach(() => {
            mockAuthService = { isAuthenticated: () => true, currentUser: {userName: 'Joe'}}
            mockVoterService = { userHasVoted: () => true}
            TestBed.configureTestingModule({
                declarations: [
                    SessionListComponent, 
                    DurationPipe
                    // CollapsibleWellComponent,
                    // UpvoteComponent
                ],
                providers: [
                    { provide: AuthService, useValue: mockAuthService},
                    { provide: VoterService, useValue: mockVoterService}
                ],
                schemas: [ // pass error of secundary components(CollapsibleWellComponent,UpvoteComponent)
                    NO_ERRORS_SCHEMA
                ]
            });

            fixture = TestBed.createComponent(SessionListComponent);
            component = fixture.componentInstance;
            debugEl = fixture.debugElement;
            element = fixture.nativeElement;
        })

        describe('initial display', () => {
            it('should have the correct title', () => {
                component.sessions = [{
                    name: 'session 1', id: 3, presenter: 'joe', duration: 1, 
                    level: 'beginner', abstract: 'gtytr',
                    voters: ['john', 'bob']
                }];
                component.filterBy = 'all'
                component.sortBy = 'name'
                component.eventId = 4
                component.ngOnChanges()
    
                fixture.detectChanges()

                // expect(element.querySelector('[well-title]').textContent).toContain('session 1')
                expect(debugEl.query(By.css('[well-title]')).nativeElement.textContent).toContain('session 1')
            })
        })
})