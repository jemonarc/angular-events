import { toBase64String } from "@angular/compiler/src/output/source_map";
import { Component, Input, Output } from "@angular/core";
import { EventEmitter } from "events";
import { IEvent } from ".";


@Component({
    selector: 'event-thumbnail',
    template: `   
    <div [routerLink]="['/events', event.id]" class="well hoverwell thumbnail">
        <h3>{{event?.name | uppercase}}</h3>
        <div>Date: {{event?.date | date:'shortDate'}}</div>
        <div [style.color]="event?.time === '8:00 am' ? '#003300' : '#bbb'">Price: {{event?.price | currency:'USD'}}</div>
        <div [ngClass]="getStartTimeClass()" [ngSwitch]="event?.time">
            Time: {{event?.time}}
            <span *ngSwitchCase="'8:00 am'"> Early start</span>
            <span *ngSwitchCase="'10:00 am'"> Late start</span>
            <span *ngSwitchDefault> Normal start</span>
        </div>
        <div [hidden]="!event?.location">
            <span>Location: {{event?.location?.address}}</span>
        </div>
        <button class="btn btn-primary" (click)="handleClickMe()">Click me!</button>
    </div>
    `,
    styles: [`
        .green { color: #003300 !important;}
        .bold { font-weight: bold;}
        .thumbnail { min-height:220px;}
        .well div { color: #bbb;}
        `] 
})

export class EventThumbnailComponent {
    @Input() event: IEvent
    @Output() eventClick = new EventEmitter()

    someProperty:any = "Child value"

    getStartTimeClass() {
        if (this.event && this.event.time === '8:00 am')
            return ['green',' bold']
        return []
    }

    handleClickMe(){
        console.log('clicked')
        this.eventClick.emit('Clickkk')
    }

    logFoo() {
        console.log('foo')
    }
}